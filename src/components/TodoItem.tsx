import { FC } from "react";
import { Todo } from "../models/todoTypes";

type TodoItemProps = {
    todo: Todo;
}

const TodoItem: FC<TodoItemProps> = ({ todo }) => {

    return (
        <div className="block w-1/3 grow-0 shrink-0 p-1 h-auto">
            <div className="p-4 bg-white h-full max-h-full max-w-full overflow-hidden w-full border border-gray-200 rounded-lg shadow hover:bg-gray-100">
                <h5 className="mb-2 text-md font-bold tracking-tight text-gray-900 truncate max-w-full">{ todo.title }</h5>
                <p className="font-normal text-gray-700">{ todo.description }</p>
            </div>
        </div>
    );
}

export default TodoItem;
import { useRef, FC } from 'react'
import { SubmitHandler, useForm } from 'react-hook-form';
import { Todo } from '../models/todoTypes';
import { todoListAtom } from '../state/atoms';
import { useSetRecoilState } from 'recoil';

const TodoForm: FC = () => {

    const setTodos = useSetRecoilState(todoListAtom);
    
    const { register, handleSubmit, reset } = useForm<Todo>({
        defaultValues: {
            title: '',
            description: '',
            isDone: false,
        }
    });

    const onSubmit: SubmitHandler<Todo> = (data) => {
        setTodos(prev => [...prev, data]);
        reset();
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)} className="w-full flex items-end gap-5 mt-5">
            <div className="basis-1/4">
                <label htmlFor="title">Title</label>
                <input id="title" {...register("title")} />
            </div>

            <div className="grow">
                <label htmlFor="description">Description</label>
                <input id="description" {...register("description")} />
            </div>

            <button className="shrink-0">Add new</button>
        </form>
    )
}

export default TodoForm;

import { FC } from 'react';
import { searchAtom } from "../state/atoms";
import { useSetRecoilState } from "recoil";
import { SubmitHandler, useForm } from "react-hook-form";

const SearchBar: FC = () => {

    const setSearch = useSetRecoilState(searchAtom);

    const { register, handleSubmit } = useForm<{ needle: string }>();
  
    const onSubmit: SubmitHandler<{ needle: string }> = (data) => {
      setSearch(data.needle);
    }

    return <form onSubmit={handleSubmit(onSubmit)} className="flex gap-2 w-full">
        <input className="grow" placeholder="Quick search..."  {...register("needle")} />
        <button className="alt-btn shrink-0">Search</button>
    </form>
}

export default SearchBar;
import { FC } from "react";
import TodoItem from "./TodoItem";
import { useRecoilValue } from "recoil";
import { filteredTodosSelector } from "../state/selectors";
import SearchBar from "./Searchbar";

const TodoList: FC = () => {
  const todos = useRecoilValue(filteredTodosSelector);

  return (
    <div className="flex flex-col mt-16">
      <h3>Todo list items</h3>
      <SearchBar />
      <div className="w-full flex flex-wrap mt-2">
        { todos.map((todo) => <TodoItem todo={todo} />) }
        { todos.length === 0 && <p className="text-gray-500 mt-5">No todos found...</p>}
      </div>
    </div>
  );
}

export default TodoList;

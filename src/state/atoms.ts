import { Todo } from "../models/todoTypes";
import { atom } from 'recoil';

export const todoListAtom = atom<Todo[]>({
    key: 'todoList',
    default: [
        { title: 'Grocery Shopping', description: 'Purchase the items on the grocery list for the week, including fruits, vegetables, meats, and snacks.', isDone: false },
        { title: 'Report for Meeting', description: 'Finish writing the report on the marketing campaign results and present it at the meeting on Friday. The report should include graphs and data analysis.', isDone: false },
        { title: 'Dentist Appointment', description: 'Call the dentists office and schedule a check-up and cleaning appointment for next week. Dont forget to confirm the date and time.', isDone: false },
        { title: 'Book Club Reading', description: 'Finish reading chapter 5 of the selected book for the book club and take notes on important details and themes to discuss at the meeting.', isDone: false },
        { title: 'House Cleaning and Laundry', description: 'Clean the house thoroughly, including vacuuming, dusting, and organizing. Also, do the laundry, fold and put away clothes, and iron if necessary.', isDone: false },
    ],
});

export const searchAtom = atom<string>({
    key: 'search',
    default: '',
});


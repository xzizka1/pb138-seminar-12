import TodoForm from './components/TodoForm';
import TodoList from './components/TodoList';

function App() {

    return (
        <div className="flex flex-col items-center py-10">
            <div className="w-[50rem]">
                <h1>Todo List App</h1>
                <div className="w-full">
                    <TodoForm />
                    <TodoList />
                </div>
            </div>
        </div>
    );
}

export default App;

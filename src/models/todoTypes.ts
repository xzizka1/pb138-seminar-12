export type Todo = {
    title: string;
    description: string;
    isDone: boolean;
}
